import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PokemonListComponent } from './components/pokemon-list/pokemon-list.component';
import { HttpClientModule } from '@angular/common/http';
import { PokemonImageComponent } from './components/pokemon-image/pokemon-image.component';
import { PokedexComponent } from './components/pokedex/pokedex.component';
import { PokeHomeComponent } from './components/poke-home/poke-home.component';

@NgModule({
  declarations: [
    AppComponent,
    PokemonListComponent,
    PokemonImageComponent,
    PokedexComponent,
    PokeHomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
