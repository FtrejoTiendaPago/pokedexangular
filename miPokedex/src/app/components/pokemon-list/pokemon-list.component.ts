import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { PokemonApiService } from '../../services/pokemon-api.service'
@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.scss']
})
export class PokemonListComponent implements OnInit {

  constructor(private pokemonApiService: PokemonApiService) { }

  pokemones: any[] = [];
  @Output() pokemon = new EventEmitter<any>();
  selectedPokemon: any = null;
  index: number = 0;
  pokemonName: string = "";

  get pokeListGet() {
    var filter = this.pokemones;
    if (this.pokemonName != "") {
      filter = filter.filter((item: any) => { return item.name.includes(this.pokemonName) })
      if (this.index > filter.length) {
        this.index = filter.length;
      }
    }
    filter = filter.slice(this.index, this.index + 10);
    for (var i = 10 - filter.length; i > 0; i--) {
      filter.push({ name: "-" })
    }
    return filter;
  }

  ngOnInit(): void {
    this.pokemon.emit(null);
    this.getPokemons();
  }

  private getPokemons() {
    this.pokemonApiService.getPokemons(0, 100000).subscribe((response: any) => {
      this.pokemones = response.results;

    })
  }

  onClick(item: any) {
    this.selectedPokemon = item;
    this.pokemon.emit(item);
  }

  next() {
    this.index += 10
  }

  preview() {
    this.index -= 10
  }

  change(event: any) {
    this.index = 0;
    this.selectedPokemon = null;
    this.pokemon.emit(null);
    this.pokemonName = event.target.value;
  }
}
