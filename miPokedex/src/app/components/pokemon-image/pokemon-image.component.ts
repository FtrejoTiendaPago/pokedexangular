import { Component, Input, OnInit } from '@angular/core';
import { PokemonApiService } from '../../services/pokemon-api.service'
@Component({
  selector: 'app-pokemon-image',
  templateUrl: './pokemon-image.component.html',
  styleUrls: ['./pokemon-image.component.scss']
})
export class PokemonImageComponent implements OnInit {

  constructor(private pokemonApiService: PokemonApiService) { }

  @Input() set pokemon(value: any) {
    if (value != null) {
      this.pokemonName = value.name;
    } else {
      this.pokemonName = null;
    }
    this.loadPokemonImage();
  }
  pokemonName: string | null = null;
  imageUrl: string = "";
  ngOnInit(): void {
    this.pokemonName = "";

  }

  loadPokemonImage() {
    if (this.pokemonName != null) {
      this.pokemonApiService.getPokemon(this.pokemonName ?? '').subscribe((result: any) => {
        this.imageUrl = result.sprites.front_default;
      })
    }else{
      this.imageUrl =  "";
    }
  }

}
