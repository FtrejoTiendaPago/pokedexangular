Encapsular los dos componentes en un componente nuevo PokeDex
	ng generate component components/pokedex
Mover la vista y el codigo de app al nuevo componente

Crear un componente home
	ng generate component components/pokeHome
Crear las rutas de home y de PokeDex en app-routing.module.ts
	import { PokeHomeComponent } from './components/poke-home/poke-home.component';
	import { PokedexComponent } from './components/pokedex/pokedex.component';

	const routes: Routes = [
	{ path: 'home', component: PokeHomeComponent },
	{ path: 'pokedex', component: PokedexComponent },
	{
		path: '', redirectTo: '/home', pathMatch: 'full'
	}
	, {
		path: '**', redirectTo: '/home', pathMatch: 'full'
	}
	];

Añádir un menu que se comparta en toda la app

<div class="menu">
    <div class="item" (click)="navigateTo('home')">Home</div>
    <div class="item" (click)="navigateTo('pokedex')">Pokedex</div>
</div>
<router-outlet></router-outlet>
	
Navegar entre rutas
  navigateTo(route:string){
    this.router.navigate([route]);
  }
  
 Agregar estilos al menu
 .menu {
    display: flex;
    background-color: chocolate;
    height: 44px;
    justify-content: flex-start;
    align-items: center;
    padding-left: 8px;
    .item {
        font-size: x-large;
        color: white;
        font-weight: bolder;
        border-right-color: white;
        border-right-width: 2px;
        border-right-style: solid;
        padding-right: 7px;
        padding-left: 7px;

        &:hover{
            color: rgb(255, 217, 185);
            cursor: pointer;
            font-weight: bolder;
            text-shadow: rgb(158, 152, 152);
        }
    }
}